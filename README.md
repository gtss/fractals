# Fractals in Python

The following code are all referenced from [itsliterallymonique's](https://medium.com/nerd-for-tech/programming-fractals-in-python-d42db4e2ed33) write-up.

Each of the links to their individual git repositories are as follows:

1. [The Koch Fractal](https://github.com/itsliterallymonique/Koch-Fractal-o_O.git)
2. [The Barnsley Fern](https://github.com/itsliterallymonique/Barnsley-Fern)
3. [The Mandelbrot Set](https://github.com/itsliterallymonique/Mandelbrot-Set.git)

# Requirements

For this exercise, we will create a new environment.
This exercise assumes an installation of conda or Anaconda is present on the system.
We will name the environment "fractals". Do that with:

    conda create --name fractals

We then need to install some packages that will be used to build the fractals.
If you want to try a minimal system, run:

    conda install numpy matplotlib

If instead you wish to clone the setup in this repository, do:

    conda create --name fractals --file requirements.txt